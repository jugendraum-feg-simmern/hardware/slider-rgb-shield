EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Driver_LED:PCA9685PW U1
U 1 1 5FF0DB4D
P 3550 2200
F 0 "U1" H 4000 1250 50  0000 C CNN
F 1 "PCA9685PW" H 3550 2200 50  0000 C CNN
F 2 "Package_SO:TSSOP-28_4.4x9.7mm_P0.65mm" H 3575 1225 50  0001 L CNN
F 3 "http://www.nxp.com/documents/data_sheet/PCA9685.pdf" H 3150 2900 50  0001 C CNN
	1    3550 2200
	1    0    0    -1  
$EndComp
$Comp
L Transistor_Array:ULN2003 U5
U 1 1 5FF0F028
P 6500 1800
F 0 "U5" H 6750 1250 50  0000 C CNN
F 1 "ULN2003" H 6500 2350 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 6550 1250 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/uln2003a.pdf" H 6600 1600 50  0001 C CNN
	1    6500 1800
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP8
U 1 1 5FF10D6A
P 1650 2400
F 0 "JP8" H 1800 2450 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 1650 2514 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 1650 2400 50  0001 C CNN
F 3 "~" H 1650 2400 50  0001 C CNN
	1    1650 2400
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP9
U 1 1 5FF115FE
P 1650 2500
F 0 "JP9" H 1800 2550 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 1650 2614 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 1650 2500 50  0001 C CNN
F 3 "~" H 1650 2500 50  0001 C CNN
	1    1650 2500
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP10
U 1 1 5FF125F0
P 1650 2600
F 0 "JP10" H 1750 2650 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 1650 2714 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 1650 2600 50  0001 C CNN
F 3 "~" H 1650 2600 50  0001 C CNN
	1    1650 2600
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP11
U 1 1 5FF12A65
P 1650 2700
F 0 "JP11" H 1750 2750 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 1650 2814 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 1650 2700 50  0001 C CNN
F 3 "~" H 1650 2700 50  0001 C CNN
	1    1650 2700
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP12
U 1 1 5FF12FBA
P 1650 2800
F 0 "JP12" H 1750 2850 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 1650 2914 50  0001 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 1650 2800 50  0001 C CNN
F 3 "~" H 1650 2800 50  0001 C CNN
	1    1650 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R65
U 1 1 5FF14A32
P 2600 3100
F 0 "R65" V 2600 3050 50  0000 L CNN
F 1 "10k" H 2550 2900 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 2600 3100 50  0001 C CNN
F 3 "~" H 2600 3100 50  0001 C CNN
	1    2600 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R64
U 1 1 5FF14C11
P 2450 3100
F 0 "R64" V 2450 3050 50  0000 L CNN
F 1 "10k" H 2400 2900 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 2450 3100 50  0001 C CNN
F 3 "~" H 2450 3100 50  0001 C CNN
	1    2450 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R63
U 1 1 5FF14E24
P 2300 3100
F 0 "R63" V 2300 3050 50  0000 L CNN
F 1 "10k" H 2250 2900 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 2300 3100 50  0001 C CNN
F 3 "~" H 2300 3100 50  0001 C CNN
	1    2300 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R62
U 1 1 5FF15086
P 2150 3100
F 0 "R62" V 2150 3050 50  0000 L CNN
F 1 "10k" H 2100 2900 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 2150 3100 50  0001 C CNN
F 3 "~" H 2150 3100 50  0001 C CNN
	1    2150 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 2800 2600 2800
Wire Wire Line
	2600 2800 2600 3000
Wire Wire Line
	2850 2700 2450 2700
Wire Wire Line
	2450 2700 2450 3000
Wire Wire Line
	2850 2600 2300 2600
Wire Wire Line
	2300 2600 2300 3000
Wire Wire Line
	2850 2500 2150 2500
Wire Wire Line
	2150 2500 2150 3000
$Comp
L Device:R_Small R61
U 1 1 5FF17A3A
P 2000 3100
F 0 "R61" V 2000 3050 50  0000 L CNN
F 1 "10k" H 1950 2900 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 2000 3100 50  0001 C CNN
F 3 "~" H 2000 3100 50  0001 C CNN
	1    2000 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 3000 2000 2400
Wire Wire Line
	2000 2400 2850 2400
Wire Wire Line
	1800 2400 2000 2400
Connection ~ 2000 2400
Wire Wire Line
	1800 2500 2150 2500
Connection ~ 2150 2500
Wire Wire Line
	1800 2600 2300 2600
Connection ~ 2300 2600
Wire Wire Line
	1800 2700 2450 2700
Connection ~ 2450 2700
Wire Wire Line
	1800 2800 2600 2800
Connection ~ 2600 2800
Wire Wire Line
	1400 2400 1500 2400
Connection ~ 1400 2400
Wire Wire Line
	1400 2400 1400 2300
Wire Wire Line
	1400 2500 1500 2500
Connection ~ 1400 2500
Wire Wire Line
	1400 2500 1400 2400
Wire Wire Line
	1400 2600 1500 2600
Connection ~ 1400 2600
Wire Wire Line
	1400 2600 1400 2500
Wire Wire Line
	1400 2700 1500 2700
Connection ~ 1400 2700
Wire Wire Line
	1400 2700 1400 2600
Wire Wire Line
	1400 2800 1500 2800
Wire Wire Line
	1400 2800 1400 2700
$Comp
L power:+5V #PWR0144
U 1 1 5FF20557
P 1400 2300
F 0 "#PWR0144" H 1400 2150 50  0001 C CNN
F 1 "+5V" H 1415 2473 50  0000 C CNN
F 2 "" H 1400 2300 50  0001 C CNN
F 3 "" H 1400 2300 50  0001 C CNN
	1    1400 2300
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0145
U 1 1 5FF21328
P 3550 1150
F 0 "#PWR0145" H 3550 1000 50  0001 C CNN
F 1 "+5V" H 3565 1323 50  0000 C CNN
F 2 "" H 3550 1150 50  0001 C CNN
F 3 "" H 3550 1150 50  0001 C CNN
	1    3550 1150
	1    0    0    -1  
$EndComp
Text HLabel 2750 1500 0    50   Input ~ 0
SDA
Text HLabel 2750 1600 0    50   Input ~ 0
SCL
$Comp
L power:GND #PWR0146
U 1 1 5FF2236F
P 2750 1900
F 0 "#PWR0146" H 2750 1650 50  0001 C CNN
F 1 "GND" H 2755 1727 50  0000 C CNN
F 2 "" H 2750 1900 50  0001 C CNN
F 3 "" H 2750 1900 50  0001 C CNN
	1    2750 1900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0147
U 1 1 5FF226F3
P 1900 3300
F 0 "#PWR0147" H 1900 3050 50  0001 C CNN
F 1 "GND" H 1905 3127 50  0000 C CNN
F 2 "" H 1900 3300 50  0001 C CNN
F 3 "" H 1900 3300 50  0001 C CNN
	1    1900 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0148
U 1 1 5FF23447
P 3550 3350
F 0 "#PWR0148" H 3550 3100 50  0001 C CNN
F 1 "GND" H 3555 3177 50  0000 C CNN
F 2 "" H 3550 3350 50  0001 C CNN
F 3 "" H 3550 3350 50  0001 C CNN
	1    3550 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 1500 2850 1500
Wire Wire Line
	2750 1600 2850 1600
Wire Wire Line
	2750 1900 2750 1800
Wire Wire Line
	2750 1700 2850 1700
Wire Wire Line
	2750 1800 2850 1800
Connection ~ 2750 1800
Wire Wire Line
	2750 1800 2750 1700
Wire Wire Line
	1900 3300 1900 3250
Wire Wire Line
	1900 3250 2000 3250
Wire Wire Line
	2600 3200 2600 3250
Connection ~ 2600 3250
Wire Wire Line
	2600 3250 2750 3250
Wire Wire Line
	2450 3200 2450 3250
Connection ~ 2450 3250
Wire Wire Line
	2450 3250 2600 3250
Wire Wire Line
	2300 3200 2300 3250
Connection ~ 2300 3250
Wire Wire Line
	2300 3250 2450 3250
Wire Wire Line
	2150 3200 2150 3250
Connection ~ 2150 3250
Wire Wire Line
	2150 3250 2300 3250
Wire Wire Line
	2000 3200 2000 3250
Connection ~ 2000 3250
Wire Wire Line
	2000 3250 2150 3250
Wire Wire Line
	3550 3300 3550 3350
Text HLabel 7000 1600 2    50   Output ~ 0
R1
Text HLabel 7000 1800 2    50   Output ~ 0
B1
Text HLabel 7000 1700 2    50   Output ~ 0
G1
Text HLabel 7000 1900 2    50   Output ~ 0
R2
Text HLabel 7000 2000 2    50   Output ~ 0
G2
Text HLabel 7000 2100 2    50   Output ~ 0
B2
Text HLabel 7000 3100 2    50   Output ~ 0
R3
Text HLabel 7000 3200 2    50   Output ~ 0
G3
Text HLabel 7000 3300 2    50   Output ~ 0
B3
Text HLabel 7000 3400 2    50   Output ~ 0
R4
Text HLabel 7000 3500 2    50   Output ~ 0
G4
Text HLabel 7000 3600 2    50   Output ~ 0
B4
Wire Wire Line
	6900 1600 7000 1600
Wire Wire Line
	6900 1700 7000 1700
Wire Wire Line
	6900 1800 7000 1800
Wire Wire Line
	6900 1900 7000 1900
Wire Wire Line
	6900 2000 7000 2000
Wire Wire Line
	6900 2100 7000 2100
Wire Wire Line
	6900 3100 7000 3100
Wire Wire Line
	6900 3200 7000 3200
Wire Wire Line
	6900 3300 7000 3300
Wire Wire Line
	6900 3400 7000 3400
Wire Wire Line
	6900 3500 7000 3500
Wire Wire Line
	6900 3600 7000 3600
$Comp
L power:GND #PWR0149
U 1 1 5FF38176
P 6500 2450
F 0 "#PWR0149" H 6500 2200 50  0001 C CNN
F 1 "GND" H 6350 2400 50  0000 C CNN
F 2 "" H 6500 2450 50  0001 C CNN
F 3 "" H 6500 2450 50  0001 C CNN
	1    6500 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0150
U 1 1 5FF38605
P 6500 3950
F 0 "#PWR0150" H 6500 3700 50  0001 C CNN
F 1 "GND" H 6350 3900 50  0000 C CNN
F 2 "" H 6500 3950 50  0001 C CNN
F 3 "" H 6500 3950 50  0001 C CNN
	1    6500 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 3950 6500 3900
Wire Wire Line
	6500 2450 6500 2400
Wire Wire Line
	3550 1150 3550 1200
Text HLabel 7050 1400 2    50   Input ~ 0
V_LED
$Comp
L Device:C C7
U 1 1 601BFA7F
P 1500 4500
F 0 "C7" H 1615 4546 50  0000 L CNN
F 1 "10u" H 1615 4455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 1538 4350 50  0001 C CNN
F 3 "~" H 1500 4500 50  0001 C CNN
	1    1500 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 601BFC56
P 1900 4500
F 0 "C8" H 2015 4546 50  0000 L CNN
F 1 "100n" H 2015 4455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 1938 4350 50  0001 C CNN
F 3 "~" H 1900 4500 50  0001 C CNN
	1    1900 4500
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0151
U 1 1 601BFF5E
P 1500 4250
F 0 "#PWR0151" H 1500 4100 50  0001 C CNN
F 1 "+5V" H 1515 4423 50  0000 C CNN
F 2 "" H 1500 4250 50  0001 C CNN
F 3 "" H 1500 4250 50  0001 C CNN
	1    1500 4250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0152
U 1 1 601C03BA
P 1900 4250
F 0 "#PWR0152" H 1900 4100 50  0001 C CNN
F 1 "+5V" H 1915 4423 50  0000 C CNN
F 2 "" H 1900 4250 50  0001 C CNN
F 3 "" H 1900 4250 50  0001 C CNN
	1    1900 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0153
U 1 1 601C0632
P 1500 4750
F 0 "#PWR0153" H 1500 4500 50  0001 C CNN
F 1 "GND" H 1505 4577 50  0000 C CNN
F 2 "" H 1500 4750 50  0001 C CNN
F 3 "" H 1500 4750 50  0001 C CNN
	1    1500 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0154
U 1 1 601C0A02
P 1900 4750
F 0 "#PWR0154" H 1900 4500 50  0001 C CNN
F 1 "GND" H 1905 4577 50  0000 C CNN
F 2 "" H 1900 4750 50  0001 C CNN
F 3 "" H 1900 4750 50  0001 C CNN
	1    1900 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 4250 1500 4350
Wire Wire Line
	1900 4250 1900 4350
Wire Wire Line
	1900 4650 1900 4750
Wire Wire Line
	1500 4650 1500 4750
Wire Wire Line
	6900 1400 7050 1400
Text HLabel 7100 2900 2    50   Input ~ 0
V_LED
Wire Wire Line
	6900 2900 7100 2900
Wire Wire Line
	2850 2900 2750 2900
Wire Wire Line
	2750 2900 2750 3250
$Comp
L Transistor_Array:ULN2003 U6
U 1 1 5FF0FCFC
P 6500 3300
F 0 "U6" H 6750 2750 50  0000 C CNN
F 1 "ULN2003" H 6500 3876 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 6550 2750 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/uln2003a.pdf" H 6600 3100 50  0001 C CNN
	1    6500 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 1700 4500 1700
Wire Wire Line
	4250 1800 4500 1800
Wire Wire Line
	4250 1900 4500 1900
Wire Wire Line
	4500 2000 4250 2000
Wire Wire Line
	4250 2100 4500 2100
Wire Wire Line
	4500 2200 4250 2200
Wire Wire Line
	4250 2300 4500 2300
Wire Wire Line
	4500 2400 4250 2400
Wire Wire Line
	4500 2500 4250 2500
Wire Wire Line
	4500 2600 4250 2600
Wire Wire Line
	4500 2700 4250 2700
Wire Wire Line
	4500 2800 4250 2800
Text Label 4500 1700 2    50   ~ 0
PWM2
Text Label 4500 1800 2    50   ~ 0
PWM3
Text Label 4500 1900 2    50   ~ 0
PWM4
Text Label 4500 2000 2    50   ~ 0
PWM5
Text Label 4500 2200 2    50   ~ 0
PWM7
Text Label 4500 2400 2    50   ~ 0
PWM9
Text Label 4500 2500 2    50   ~ 0
PWM10
Text Label 4500 2300 2    50   ~ 0
PWM8
Text Label 4500 2600 2    50   ~ 0
PWM11
Text Label 4500 2700 2    50   ~ 0
PWM12
Text Label 4500 2800 2    50   ~ 0
PWM13
Wire Wire Line
	6100 1600 5850 1600
Wire Wire Line
	5850 1700 6100 1700
Wire Wire Line
	5850 1800 6100 1800
Wire Wire Line
	5850 1900 6100 1900
Wire Wire Line
	5850 2000 6100 2000
Wire Wire Line
	5850 2100 6100 2100
Wire Wire Line
	5850 3100 6100 3100
Wire Wire Line
	5850 3200 6100 3200
Wire Wire Line
	5850 3300 6100 3300
Wire Wire Line
	5850 3400 6100 3400
Wire Wire Line
	5850 3500 6100 3500
Wire Wire Line
	5850 3600 6100 3600
Text Label 5850 3100 0    50   ~ 0
PWM7
Text Label 4500 2100 2    50   ~ 0
PWM6
Text Label 5850 3200 0    50   ~ 0
PWM6
Text Label 5850 3300 0    50   ~ 0
PWM5
Text Label 5850 3400 0    50   ~ 0
PWM4
Text Label 5850 3500 0    50   ~ 0
PWM3
Text Label 5850 3600 0    50   ~ 0
PWM2
Text Label 5850 2100 0    50   ~ 0
PWM8
Text Label 5850 2000 0    50   ~ 0
PWM9
Text Label 5850 1900 0    50   ~ 0
PWM10
Text Label 5850 1800 0    50   ~ 0
PWM11
Text Label 5850 1700 0    50   ~ 0
PWM12
Text Label 5850 1600 0    50   ~ 0
PWM13
$Comp
L Device:CP C2
U 1 1 5FEE7970
P 2300 4500
F 0 "C2" H 2418 4546 50  0000 L CNN
F 1 "CP" H 2418 4455 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.7" H 2338 4350 50  0001 C CNN
F 3 "~" H 2300 4500 50  0001 C CNN
	1    2300 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C9
U 1 1 5FEE8447
P 2700 4500
F 0 "C9" H 2818 4546 50  0000 L CNN
F 1 "CP" H 2818 4455 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.7" H 2738 4350 50  0001 C CNN
F 3 "~" H 2700 4500 50  0001 C CNN
	1    2700 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0143
U 1 1 5FEE8682
P 2300 4750
F 0 "#PWR0143" H 2300 4500 50  0001 C CNN
F 1 "GND" H 2305 4577 50  0000 C CNN
F 2 "" H 2300 4750 50  0001 C CNN
F 3 "" H 2300 4750 50  0001 C CNN
	1    2300 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0155
U 1 1 5FEE8926
P 2700 4750
F 0 "#PWR0155" H 2700 4500 50  0001 C CNN
F 1 "GND" H 2705 4577 50  0000 C CNN
F 2 "" H 2700 4750 50  0001 C CNN
F 3 "" H 2700 4750 50  0001 C CNN
	1    2700 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 4650 2700 4750
Wire Wire Line
	2300 4650 2300 4750
Wire Wire Line
	2300 4250 2300 4350
Wire Wire Line
	2700 4250 2700 4350
Text HLabel 2300 4250 1    50   Input ~ 0
V_LED
Text HLabel 2700 4250 1    50   Input ~ 0
V_LED
$EndSCHEMATC
