# Slider RGB Shield

Arduino shield with 4 sliders and RGB backlighting.

## Front

![render_front](img/render_front.png)

## Back

![render_back](img/render_back.png)
